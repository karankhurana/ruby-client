
require 'rubygems'
require 'net/ssh'
require 'pg'

HOST = '192.168.1.113'
USER = 'username'
PASS = 'password'

Net::SSH.start( HOST, USER, :password => PASS ) do |ssh|
  output = ssh.exec!('ls')
  puts output

  begin
    con = PG.connect :dbname => 'testdb', :user => 'karan'
    rs = con.exec "SELECT * FROM Cars LIMIT 5"

    rs.each do |row|
      puts "%s %s %s" % [ row['id'], row['name'], row['price'] ]
    end
  rescue PG::Error => e
    puts e.message 
  ensure
    con.close if con
  end
end



# output
#1 Audi 52642
#2 Mercedes 57127
#3 Skoda 9000
#4 Volvo 29000
#5 Bentley 350000
#
